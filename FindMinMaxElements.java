import java.util.*;

public class FindMinMaxElements {
    public static void main(String[] args) {
        Random random = new Random();
        List<Integer> randomNumbers = new ArrayList<>();

        for (int i = 0; i < 10; i++) {
            randomNumbers.add(random.nextInt(100));
        }

        testFindMinMax(randomNumbers, Collections.min(randomNumbers), Collections.max(randomNumbers));
    }

    public static <T extends Comparable<? super T>> PairOfMinMax<T> findMinMax(
            Collection<T> collection
    ) {
        if (collection == null || collection.isEmpty()) {
            throw new IllegalArgumentException("Collection is null or empty");
        }
        Iterator<T> iterator = collection.iterator();
        T min = iterator.next();
        T max = min;

        while (iterator.hasNext()) {
            T element = iterator.next();
            if (element.compareTo(min) < 0) {
                min = element;
            }
            if (element.compareTo(max) > 0) {
                max = element;
            }
        }

        return new PairOfMinMax<>(min, max);
    }

    public static class PairOfMinMax<K extends Comparable<? super K>> {
        private final K min;
        private final K max;

        public PairOfMinMax(K min, K max) {
            this.min = min;
            this.max = max;
        }

        public Object getMin() {
            return min;
        }

        public Object getMax() {
            return max;
        }
    }

    private static <T extends Comparable<? super T>> void testFindMinMax(
            Collection<T> collection,
            T expectedMin,
            T expectedMax
    ) {
        try {
            PairOfMinMax<T> minMax = findMinMax(collection);
            System.out.println("Коллекция: " + collection);
            System.out.println("Минимальный элемент: " + minMax.getMin() +
                    ", ожидаемый минимальный: " + expectedMin);
            System.out.println("Максимальный элемент: " + minMax.getMax() +
                    ", ожидаемый максимальный: " + expectedMax);
            System.out.println();
        } catch (IllegalArgumentException e) {
            if (expectedMin == null && expectedMax == null) {
                System.out.println("Числа не определенны " + e.getMessage());
            } else {
                throw e;
            }
        }
    }
}
